var Card = function(suit,value){
    this.suit = suit;
    this.value = value;
}

var deck = [];
for(var i=1;i<=13;i++){
    var a = new Card("Spades",i);
    var b = new Card("Hearts",i);
    var c = new Card("Diamonds",i);
    var d = new Card("Clubs",i);
    deck.push(a,b,c,d);
}

var times = parseInt(prompt("Enter x to shuffle x times and draw top card:"));
if (Number.isInteger(times)==false){
    alert("Please key in an integer value!");
}

var shuffle = function(x){
    var newDeck = [];
    for(var k=0;k<x;k++) {
        for (var j = 51; j >= 0; j--) {
            var index = Math.floor(Math.random() * j);
            newDeck.push(deck[index]);
            deck.splice(index,1);
        }
        deck=newDeck;
    }
    return(newDeck);
};

var topCard = shuffle(times)[0];
var topSuit = topCard.suit;
var topValue;
switch(topCard.value){
    case 1:
        topValue = "Ace";
        break;
    case 11:
        topValue = "Jack";
        break;
    case 12:
        topValue = "Queen";
        break;
    case 13:
        topValue = "King";
        break;
    default:
        topValue = topCard.value;
}

document.write("<h4>The top card is " + topValue
            + " of " + topSuit + "</h4>");
var cardValue
switch(topCard.value){
    case 1:
        document.write("<div> A");
        break;
    case 11:
        document.write("<div> J");
        break;
    case 12:
        document.write("<div> Q");
        break;
    case 13:
        document.write("<div> K");
        break;
    default:
        document.write("<div> " + topCard.value);
        break;
}
document.write("<br>");

switch(topSuit){
    case "Spades":
        document.write("<img src='spade.png'>");
        break;
    case "Hearts":
        document.write("<img src='heart.png'>");
        break;
    case "Diamonds":
        document.write("<img src='diamond.png'>");
        break;
    case "Clubs":
        document.write("<img src='club.png'>");
        break;
}
document.write("</div>");