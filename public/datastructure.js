// Numbers, Strings, Boolean and undefined are called primitives
var myArray = [];
var daysOfWeek = ["Mon","Tue","Wed","Thu","Fri","Sat","Sun"];
var gameOfThrones = ["Stark","Arryn","Hoare","Lannister","Durrandon","Highgarden","Martell"];
var taskList = [];

// Add to the back of the array
taskList.push("Learn JavaScript");
taskList.push("Learn HTML");
taskList.push("Learn CSS");

// Add to the front of the array
taskList.unshift("Learn AngularJS");
taskList.join(".");

var item = taskList.pop();

console.log(item);

for(var i=0;i<taskList.length;i++){
    console.log((i+1)+". " + taskList[i]);
}
if(taskList.length>=4)
    console.log("There's a lot to do! Phew...");
else {
    console.log("Give me more tasks!")

}
console.log(taskList);

for (var i=0; i<10;i++){
    myArray[i]=i+1;
}

/*
console.info("> ", daysOfWeek.length);
console.log(myArray + "> ", myArray.length);

for (i=0;i<daysOfWeek.length;i++){
    console.log("> " + daysOfWeek[i]);
}
var j=0;
while(j<daysOfWeek.length){
    console.log(">> " + daysOfWeek[j]);
    j++;
}
*/

